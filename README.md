This is a WordPress build that lets you use Sqlite as Database instead of MySQL, this has been made possible using the [sqlite-integration](https://wordpress.org/plugins/sqlite-integration/) plugin developed by  [kjmtsh](https://profiles.wordpress.org/kjmtsh/)

You can learn about this project by looking over [here](http://darryldias.me/blog/wordpress-with-sqlite/).